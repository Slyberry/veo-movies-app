package com.sylwesterzielinski.veo.movies.list.view

import com.sylwesterzielinski.veo.movies.domain.Movie
import com.sylwesterzielinski.veo.movies.repository.AsyncResource

internal data class ScreenMoviesListState(
    val movies: AsyncResource<List<Movie>>? = null
)
