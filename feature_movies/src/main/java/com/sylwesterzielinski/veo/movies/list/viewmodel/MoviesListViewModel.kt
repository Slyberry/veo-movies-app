package com.sylwesterzielinski.veo.movies.list.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sylwesterzielinski.veo.movies.list.view.ScreenMoviesListState
import com.sylwesterzielinski.veo.movies.repository.MoviesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
internal class MoviesListViewModel @Inject constructor(
    repository: MoviesRepository
) : ViewModel() {

    private val _state = MutableStateFlow(ScreenMoviesListState())
    val state = _state.asStateFlow()

    init {
        repository.getMovies()
            .onEach {
                _state.value = _state.value.copy(movies = it)
            }
            .launchIn(viewModelScope)
    }
}
