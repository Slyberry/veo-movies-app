package com.sylwesterzielinski.veo.movies.list.view

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.sylwesterzielinski.veo.movies.domain.Movie
import com.sylwesterzielinski.veo.movies.list.viewmodel.MoviesListViewModel
import com.sylwesterzielinski.veo.movies.repository.ErrorResource
import com.sylwesterzielinski.veo.movies.repository.LoadingResource
import com.sylwesterzielinski.veo.movies.repository.SuccessResource
import com.sylwesterzielinski.veo.movies.ui.ErrorView
import com.sylwesterzielinski.veo.movies.ui.LoadingView
import com.sylwesterzielinski.veo.movies.ui.Poster
import com.sylwesterzielinski.veo.movies.list.R

@Composable
fun ScreenMoviesList(onNavigateToDetails: (Int) -> Unit) {
    val viewModel = hiltViewModel<MoviesListViewModel>()
    val state = viewModel.state.collectAsState().value

    when (state.movies) {
        is LoadingResource -> LoadingView()
        is SuccessResource -> SuccessView(state.movies.data, onNavigateToDetails)
        is ErrorResource -> ErrorView(state.movies.error)
        null -> LoadingView()
    }
}

@Composable
private fun SuccessView(data: List<Movie>, onNavigateToDetails: (Int) -> Unit) {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(16.dp),
        contentPadding = PaddingValues(16.dp)
    ) {
        items(items = data) {
            MovieItem(it, onNavigateToDetails)
        }
    }
}

@Composable
private fun MovieItem(movie: Movie, onNavigateToDetails: (Int) -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onNavigateToDetails(movie.id) }
    ) {
        Poster(poster = movie.poster)

        Spacer(modifier = Modifier.size(16.dp))

        Column {
            Text(text = movie.title, style = MaterialTheme.typography.titleLarge)

            Spacer(modifier = Modifier.size(16.dp))

            Text(
                text = stringResource(id = R.string.rating, movie.rating),
                style = MaterialTheme.typography.bodyLarge
            )
        }
    }
}
