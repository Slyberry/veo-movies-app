package com.sylwesterzielinski.veo.movies.domain

data class Movie(
    val id: Int,
    val title: String,
    val poster: String,
    val rating: Double
)
