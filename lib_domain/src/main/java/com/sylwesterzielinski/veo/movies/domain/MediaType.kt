package com.sylwesterzielinski.veo.movies.domain

enum class MediaType {
    ALL, MOVIE, TV, PERSON
}
