package com.sylwesterzielinski.veo.movies.domain

enum class TimeWindow {
    DAY, WEEK
}
