package com.sylwesterzielinski.veo.movies.domain

data class MovieDetails(
    val title: String,
    val budget: Int,
    val adult: Boolean,
    val overview: String,
    val revenue: Int,
    val poster: String,
    val rating: Double,
    val votes: Int,
)
