package com.sylwesterzielinski.veo.movies.app

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class VeoMoviesApp : Application()
