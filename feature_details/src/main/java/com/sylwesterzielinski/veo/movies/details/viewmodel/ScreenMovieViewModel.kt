package com.sylwesterzielinski.veo.movies.details.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sylwesterzielinski.veo.movies.details.view.ScreenMovieDetailsState
import com.sylwesterzielinski.veo.movies.repository.MoviesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
internal class ScreenMovieViewModel @Inject constructor(
    repository: MoviesRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val movieId: Int = checkNotNull(savedStateHandle["movieId"])

    private val _state = MutableStateFlow(ScreenMovieDetailsState())
    val state = _state.asStateFlow()

    init {
        repository.getDetails(movieId)
            .onEach {
                _state.value = _state.value.copy(details = it)
            }
            .launchIn(viewModelScope)
    }
}
