package com.sylwesterzielinski.veo.movies.details.view

import com.sylwesterzielinski.veo.movies.domain.MovieDetails
import com.sylwesterzielinski.veo.movies.repository.AsyncResource

internal data class ScreenMovieDetailsState(
    val details: AsyncResource<MovieDetails>? = null
)
