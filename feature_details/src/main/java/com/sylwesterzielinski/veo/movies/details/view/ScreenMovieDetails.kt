package com.sylwesterzielinski.veo.movies.details.view

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.sylwesterzielinski.veo.movies.details.viewmodel.ScreenMovieViewModel
import com.sylwesterzielinski.veo.movies.domain.MovieDetails
import com.sylwesterzielinski.veo.movies.repository.ErrorResource
import com.sylwesterzielinski.veo.movies.repository.LoadingResource
import com.sylwesterzielinski.veo.movies.repository.SuccessResource
import com.sylwesterzielinski.veo.movies.ui.ErrorView
import com.sylwesterzielinski.veo.movies.ui.LoadingView
import com.sylwesterzielinski.veo.movies.ui.Poster
import com.sylwesterzielinski.veo.movies.details.R

@Composable
fun ScreenMovieDetails() {
    val viewModel = hiltViewModel<ScreenMovieViewModel>()
    val state = viewModel.state.collectAsState().value

    when (state.details) {
        is LoadingResource -> LoadingView()
        is SuccessResource -> SuccessView(state.details.data)
        is ErrorResource -> ErrorView(state.details.error)
        null -> LoadingView()
    }
}

@Composable
private fun SuccessView(data: MovieDetails) {
    Column(
        modifier = Modifier
            .padding(16.dp)
            .verticalScroll(rememberScrollState())
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,

            ) {
            Poster(poster = data.poster)

            Spacer(modifier = Modifier.size(16.dp))

            MovieInfo(data = data)
        }

        Spacer(modifier = Modifier.size(16.dp))

        Text(text = data.overview, style = MaterialTheme.typography.bodyMedium)
    }
}

@Composable
private fun MovieInfo(data: MovieDetails) {
    Column {
        Text(text = data.title, style = MaterialTheme.typography.titleLarge)

        Spacer(modifier = Modifier.size(16.dp))

        Text(
            text = stringResource(id = R.string.rating, data.rating),
            style = MaterialTheme.typography.bodyLarge
        )

        Spacer(modifier = Modifier.size(16.dp))

        Text(
            text = stringResource(id = R.string.rating_count, data.votes),
            style = MaterialTheme.typography.bodyLarge
        )
    }
}
