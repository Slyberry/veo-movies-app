package com.sylwesterzielinski.veo.movies.network

import com.sylwesterzielinski.veo.movies.network.api.NetworkApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val API_HOST = "https://api.themoviedb.org/3/"

@Module
@InstallIn(SingletonComponent::class)
internal object NetworkHiltModule {

    @Provides
    fun providesRetrofit(): NetworkApi {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        val client: OkHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()

        val retrofit = Retrofit.Builder()
            .client(client)
            .baseUrl(API_HOST)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        return retrofit.create(NetworkApi::class.java)
    }
}
