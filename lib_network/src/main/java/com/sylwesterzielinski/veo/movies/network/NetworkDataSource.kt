package com.sylwesterzielinski.veo.movies.network

import com.sylwesterzielinski.veo.movies.domain.Movie
import com.sylwesterzielinski.veo.movies.domain.MovieDetails
import com.sylwesterzielinski.veo.movies.network.api.NetworkApi
import com.sylwesterzielinski.veo.movies.network.mapper.toDomain
import javax.inject.Inject
import javax.inject.Singleton

private const val API_KEY = "f4b37e1df049788a3f75012baa793170"

@Singleton
class NetworkDataSource @Inject internal constructor(
    private val api: NetworkApi
) {

    //TODO("Add paging")
    suspend fun getMovies(): List<Movie> {
        return api.getMovies(API_KEY).results.mapNotNull {
            it.toDomain()
        }
    }

    suspend fun getDetails(id: Int): MovieDetails {
        return api.getDetails(id, API_KEY).toDomain()
    }
}
