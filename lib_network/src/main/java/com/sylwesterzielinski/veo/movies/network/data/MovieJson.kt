package com.sylwesterzielinski.veo.movies.network.data

import com.squareup.moshi.Json

data class MovieJson(
    @field:Json(name = "id")
    val id: Int?,
    @field:Json(name = "poster_path")
    val posterPath: String?,
    @field:Json(name = "title")
    val title: String?,
    @field:Json(name = "vote_average")
    val rating: Double?
)
