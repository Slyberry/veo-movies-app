package com.sylwesterzielinski.veo.movies.network.data

import com.squareup.moshi.Json

internal data class MoviesPageJson(
    @field:Json(name = "page")
    val page: Int,
    @field:Json(name = "total_pages")
    val totalPages: Int,
    @field:Json(name = "total_results")
    val totalResults: Int,
    @field:Json(name = "results")
    val results: List<MovieJson>,
)
