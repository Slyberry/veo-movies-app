package com.sylwesterzielinski.veo.movies.network.mapper

import com.sylwesterzielinski.veo.movies.domain.Movie
import com.sylwesterzielinski.veo.movies.domain.MovieDetails
import com.sylwesterzielinski.veo.movies.network.data.MovieDetailsJson
import com.sylwesterzielinski.veo.movies.network.data.MovieJson

internal fun MovieJson.toDomain(): Movie? {
    if (id == null || title == null || posterPath == null || rating == null) {
        return null
    }
    return Movie(
        id = id,
        title = title,
        poster = "https://image.tmdb.org/t/p/original$posterPath",
        rating = rating
    )
}

internal fun MovieDetailsJson.toDomain(): MovieDetails {
    return MovieDetails(
        title = title!!,
        budget = budget!!,
        adult = adult!!,
        overview = overview!!,
        revenue = revenue!!,
        poster = "https://image.tmdb.org/t/p/original$poster_path",
        rating = rating!!,
        votes = votes!!
    )
}
