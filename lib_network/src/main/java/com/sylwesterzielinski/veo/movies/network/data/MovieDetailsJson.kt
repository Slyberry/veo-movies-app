package com.sylwesterzielinski.veo.movies.network.data

import com.squareup.moshi.Json

data class MovieDetailsJson(
    @field:Json(name = "title")
    val title: String?,
    @field:Json(name = "budget")
    val budget: Int?,
    @field:Json(name = "adult")
    val adult: Boolean?,
    @field:Json(name = "overview")
    val overview: String?,
    @field:Json(name = "revenue")
    val revenue: Int?,
    @field:Json(name = "poster_path")
    val poster_path: String?,
    @field:Json(name = "vote_average")
    val rating: Double?,
    @field:Json(name = "vote_count")
    val votes: Int?
)
