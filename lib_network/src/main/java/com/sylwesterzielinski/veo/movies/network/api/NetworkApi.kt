package com.sylwesterzielinski.veo.movies.network.api

import com.sylwesterzielinski.veo.movies.network.data.MovieDetailsJson
import com.sylwesterzielinski.veo.movies.network.data.MoviesPageJson
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

internal interface NetworkApi {

//    @GET("trending/trending/{media_type}/{time_window}")
//    suspend fun getMovies(
//        @Query("api_key") apiKey: String,
//        @Path("media_type") mediaType: String,
//        @Path("time_window") timeWindow: String
//    ): MoviesPageJson

    @GET("trending/all/day")
    suspend fun getMovies(
        @Query("api_key") apiKey: String
    ): MoviesPageJson

    @GET("movie/{id}")
    suspend fun getDetails(@Path("id") id: Int, @Query("api_key") apiKey: String): MovieDetailsJson
}
