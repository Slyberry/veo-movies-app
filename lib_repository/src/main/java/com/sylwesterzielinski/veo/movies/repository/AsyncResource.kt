package com.sylwesterzielinski.veo.movies.repository

sealed class AsyncResource<T> {

    companion object {
        fun <T> createLoading() = LoadingResource<T>()
        fun <T> createSuccess(data: T) = SuccessResource(data)
        fun <T> createError(error: Throwable) = ErrorResource<T>(error)
    }
}

class LoadingResource <T> : AsyncResource<T>()
data class SuccessResource<T>(val data: T) : AsyncResource<T>()
data class ErrorResource<T>(val error: Throwable) : AsyncResource<T>()
