package com.sylwesterzielinski.veo.movies.repository

import com.sylwesterzielinski.veo.movies.domain.Movie
import com.sylwesterzielinski.veo.movies.domain.MovieDetails
import com.sylwesterzielinski.veo.movies.network.NetworkDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesRepository @Inject constructor(
    private val network: NetworkDataSource
) {

    fun getMovies(): Flow<AsyncResource<List<Movie>>> {
        return load { network.getMovies() }
    }

    fun getDetails(id: Int): Flow<AsyncResource<MovieDetails>> {
        return load { network.getDetails(id) }
    }


    private fun <T> load(block: suspend () -> T): Flow<AsyncResource<T>> {
        return flow {
            emit(AsyncResource.createLoading())
            emit(withContext(Dispatchers.IO) { AsyncResource.createSuccess(block()) })
        }.catch {
            it.printStackTrace()
            emit(AsyncResource.createError(it))
        }
    }
}
